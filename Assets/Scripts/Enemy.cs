using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE_24T2.GAD176.Brief1.Inventory
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] protected new string name;
        Item favouriteItem;

        // Start is called before the first frame update
        void Start()
        {
            favouriteItem.OutputInfoToConsole();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
