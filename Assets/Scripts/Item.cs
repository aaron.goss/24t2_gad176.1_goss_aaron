using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace SAE_24T2.GAD176.Brief1.Inventory
{
    public class Item : MonoBehaviour
    {
        // When declaring a variable, I need FOUR THINGS
        // access modifier
        // type
        // name
        // value

        [SerializeField] protected new string name;
        [SerializeField] protected int valueInDollars;
        [SerializeField] protected float weightInKilograms;

        protected void SetItemProperties(string newName, int newValueInDollars, float newWeightInKilograms)
        {
            // Set name
            name = newName;

            // Set item worth in dollars
            valueInDollars = newValueInDollars;

            // Set weight in kilograms
            weightInKilograms = newWeightInKilograms;

            OutputInfoToConsole();
        }

        public void OutputInfoToConsole()
        {
            // Output "This item has a value of X and a weight of Y."
            Debug.Log(name + " has a value of " + valueInDollars + " and a weight of " + weightInKilograms + ".");
        }
    }
}