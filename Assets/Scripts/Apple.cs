using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE_24T2.GAD176.Brief1.Inventory
{
    public class Apple : Item
    {
        [SerializeField] protected bool isPoisoned = true;

        private void Start()
        {
            SetItemProperties("Apple", 3, 0.1f);
        }
    }
}
