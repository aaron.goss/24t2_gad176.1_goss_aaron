using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE_24T2.GAD176.Brief1.Inventory
{
    public class Broomstick : Item
    {
        private void Start()
        {
            SetItemProperties("Broomstick", 10, 0.35f);
        }

        private void Sweep()
        {
            Debug.Log("Sweeping the floor!");
        }
    }
}
